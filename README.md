# Mutiplicar console app

Esta es una aplicación donde se utiliza la consola para realizar tablas de multiplicar.

Comando: ```listar```

```--base, -b=número``` => para establecer la base de la multiplicación (obligatorio)

```--limite, -l=número``` => para establecer hasta que número se multiplicará (opcional)


Comando: ```crear```

```--base, -b=número``` => para establecer la base de la multiplicación (obligatorio)

```--limite, -l=número``` => para establecer hasta que número se multiplicará (opcional)


## Ejemplo

Listar la tabla del 5 desde el 1 hasta el 20:

```
node app listar -b 5 -l 20
```


```npm install```
Para instalar los node_modules