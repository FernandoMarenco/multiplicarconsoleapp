const argv = require('./config/yargs').argv;
var colors = require('colors/safe');

const { crearArchivo, listarTabla } = require('./multiplicar/multiplicar');

//let base = '10';

/*console.log(process.argv);

let argv2 = process.argv;
let parametro = argv[2];

let base = parametro.split('=')[1];*/

//console.log('Limite', argv.limite);

let comando = argv._[0];

switch (comando) {
    case 'listar':
        console.log('Listar');
        listarTabla(argv.base, argv.limite);
        break;

    case 'crear':
        console.log('Crear');
        crearArchivo(argv.base, argv.limite)
            .then(archivo => console.log(`Archivo creado:`, colors.rainbow(archivo)))
            .catch(err => {
                console.log(err);
            });
        break;

    default:
        console.log('no reconocido');
        break;
}