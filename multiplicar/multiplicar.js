const fs = require('fs'); //propia de node
var colors = require('colors');
// const fs = require('express'); //no es propia de node
// const fs = require('./fs'); //archivos del proyectos

let listarTabla = (base, limite = 10) => {

    if (!Number(base) || !Number(limite)) {
        reject(`Debes insertar un número`);
        return;
    }

    console.log("===============".green);
    console.log(`La tabla del ${base}`.blue);
    console.log("===============".red);

    for (let i = 1; i <= limite; i++) {
        console.log(`${base} * ${i} = ${base*i}`);
    }

}

let crearArchivo = (base, limite = 10) => {
    return new Promise((resolve, reject) => {

        if (!Number(base) || !Number(limite)) {
            reject(`El valor introducido ${base} ${limite} no es un número`);
            return;
        }

        let data = '';

        for (let i = 1; i <= limite; i++) {
            data += `${base} * ${i} = ${base*i}\n`;
        }

        fs.writeFile(`tablas/tabla${base}-limite${limite}.txt`, data, (err) => {
            if (err)
                reject(err);
            else
                resolve(`tabla${base}-limite${limite}.txt`);
        });


    });
}

module.exports = {
    crearArchivo,
    listarTabla
}